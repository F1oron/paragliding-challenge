# coding: utf-8

import re

RE_OZI_VERSION = re.compile("OziExplorer Waypoint File Version ([0-9]\.[0-9])")
wpt_parameters_v1_1 = {"index": 0, "name": 1, "latitude": 2, "longitude": 3, "date": 4, "symbol": 5, "status": 6,
                       "mapDisplayFormat": 7, "foregroundColor": 8, "backgroundColor": 9, "description": 10,
                       "pointerDirection": 11, "garminDisplayFormat": 12, "proximityDistance": 13, "altitude": 14,
                       'fontSize': 15, 'fontStyle': 16, 'symbolSize': 17}


def loads(wpt_file):
    lines = wpt_file.split('\n')

    match = RE_OZI_VERSION.match(lines[0])
    if not match:
        raise RuntimeError("Ozi version is not available in specified file")
    version = match.group(1)

    wpt_object = {"ozi_version": version, "geodesic_system": lines[1].strip()}

    waypoints = []

    for line in lines[4:]:
        waypoint = parse_waypoint(line)
        if waypoint:
            waypoints.append(parse_waypoint(line))

    wpt_object["waypoint"] = waypoints
    return wpt_object


def parse_waypoint(line):
    parameter_list = line.split(',')
    if len(parameter_list) < 15:
        return None
    latitude = float(parameter_list[wpt_parameters_v1_1["latitude"]])
    longitude = float(parameter_list[wpt_parameters_v1_1["longitude"]])
    altitude = int(parameter_list[wpt_parameters_v1_1["altitude"]])
    description = parameter_list[wpt_parameters_v1_1["description"]].strip()
    name = parameter_list[wpt_parameters_v1_1["name"]].strip()

    index = int(parameter_list[wpt_parameters_v1_1["index"]])
    proximity = int(parameter_list[wpt_parameters_v1_1["proximityDistance"]])
    symbol = int(parameter_list[wpt_parameters_v1_1["symbol"]])
    status = int(parameter_list[wpt_parameters_v1_1["status"]])
    map_display_format = int(parameter_list[wpt_parameters_v1_1["mapDisplayFormat"]])
    foreground_color = int(parameter_list[wpt_parameters_v1_1["foregroundColor"]])
    background_color = int(parameter_list[wpt_parameters_v1_1["backgroundColor"]])
    pointer_direction = int(parameter_list[wpt_parameters_v1_1["pointerDirection"]])
    garmin_display_format = int(parameter_list[wpt_parameters_v1_1["garminDisplayFormat"]])

    waypoint = {"index": index, "name": name, "latitude": latitude, "longitude": longitude,
                "symbol": symbol, "status": status, "mapDisplayFormat": map_display_format,
                "foregroundColor": foreground_color, "backgroundColor": background_color, "description": description,
                "pointerDirection": pointer_direction, "garminDisplayFormat": garmin_display_format,
                "proximity": proximity, "altitude": altitude}

    if wpt_parameters_v1_1['fontSize'] < len(parameter_list):
        waypoint['fontSize'] = int(parameter_list[wpt_parameters_v1_1["fontSize"]])

    if wpt_parameters_v1_1['fontStyle'] < len(parameter_list):
        waypoint['fontStyle'] = int(parameter_list[wpt_parameters_v1_1["fontStyle"]])

    if wpt_parameters_v1_1['symbolSize'] < len(parameter_list):
        waypoint['symbolSize'] = int(parameter_list[wpt_parameters_v1_1["symbolSize"]])

    date = parameter_list[wpt_parameters_v1_1["date"]].strip()
    if date:
        waypoint['date'] = date

    return waypoint
