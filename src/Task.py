# coding: utf-8
from Turnpoint import TaskPoint, PointType

TAKEOFF = 0x1
LANDING = 0x2
TURNPOINT = 0x4


class Task:
    def __init__(self, points):
        self.points = points

    def __repr__(self):
        s = ""
        for point in self.points:
            s += "\t{}\n".format(point)
        return s

    def __eq__(self, other):
        result = True
        if len(self.points) != len(other.points):
            return False
        for index in range(len(self.points)):
            if self.points != other.points:
                result = False
        return result

    def sort_point_by_category(self):
        sorted_task = []
        sorted_task.extend(list(self.filter_point_type(PointType.TAKEOFF)))
        sorted_task.extend(list(self.filter_point_type(PointType.LANDING)))
        sorted_task.extend(list(self.filter_point_type(PointType.TURNPOINT)))
        self.points = sorted_task
        return self

    def provide_index_to_point(self):
        index = 0
        for point in self.points:
            point.set_index(index)
            index += 1
        return self

    def add_score_to_empty_turnpoints(self):
        for turnpoint in self.filter_point_type(PointType.TURNPOINT):
            if turnpoint.is_score_empty():
                distance_to_takeoff_list = []
                for takeoff in self.filter_point_type(PointType.TAKEOFF):
                    distance_to_takeoff_list.append(turnpoint.get_distance_to_point(takeoff))
                turnpoint.add_score(float('{:.2}'.format(min(distance_to_takeoff_list) / 1000)))
        return self

    def build_object(self, type_bitmask=TURNPOINT):
        task_object = []
        if (type_bitmask & TAKEOFF) == TAKEOFF:
            task_object.extend(list(map(lambda x: x.build_object(), self.filter_point_type(PointType.TAKEOFF))))
        if (type_bitmask & LANDING) == LANDING:
            task_object.extend(list(map(lambda x: x.build_object(), self.filter_point_type(PointType.LANDING))))
        if (type_bitmask & TURNPOINT) == TURNPOINT:
            task_object.extend(list(map(lambda x: x.build_object(), self.filter_point_type(PointType.TURNPOINT))))
        return task_object

    def filter_point_type(self, point_type):
        return filter(lambda x: x.pointType == point_type, self.points)
