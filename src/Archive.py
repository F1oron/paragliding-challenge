# coding: utf-8
from zipfile import ZipFile


class ZipType:
    def __init__(self, file_path):
        self.file_path = file_path

    def extract(self):
        with ZipFile(self.file_path, 'r') as zip_file:
            zip_file.extractall()
            return self.file_path[:-4]


def ArchiveFactory(file_path):
    output_type = {
        ".zip": ZipType
    }
    return output_type[file_path[file_path.rfind('.'):]](file_path)
