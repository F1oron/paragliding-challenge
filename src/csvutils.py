# coding: utf8

import requests
import re

RE_ETHERCALC_URL = re.compile("^(https?:\/\/)([a-zA-Z0-9\.\-]+)\/([a-zA-Z0-9\-]+)$")


def convert_csv_to_wpt(csv_file_path, wpt_file_path):
    with open(csv_file_path, 'r') as csvfile:
        csv_file_content = csvfile.read()

    write_file(wpt_file_path, build_wpt_content_from_csv(csv_file_content))


def download_csv_from_ethercalc_and_save_it(ethercalc_url, wpt_file_path):
    write_file(wpt_file_path, download_csv_file_from_ethercalc(ethercalc_url))


def download_csv_file_from_ethercalc(ethercalc_url):
    x = requests.get(find_ethercalc_url_api_from_page_url(ethercalc_url))
    if x.status_code == 200:
        return x.text
    else:
        raise RuntimeError(f"Connection to ethercalc page failed with code {x.status_code}")


def find_ethercalc_url_api_from_page_url(ethercalc_url):
    match = RE_ETHERCALC_URL.match(ethercalc_url)
    if not match:
        raise RuntimeError(f"URL to ethercalc is not valid : {ethercalc_url}")
    return f"{match.group(1)}{match.group(2)}/_/{match.group(3)}/csv"


def write_file(file_path, content):
    with open(file_path, 'w') as file:
        file.write(content)


def build_wpt_content_from_csv(csv_content):
    content = csv_content[csv_content.find('\n')+1:]
    
    wpt_file_header =  "OziExplorer Waypoint File Version 1.1\n"\
                        "WGS 84\n"\
                        "Reserved 2\n"\
                        "Reserved 3\n"

    return wpt_file_header + remove_starting_and_ending_quotes(content)


def remove_starting_and_ending_quotes(content):
    rows = []
    for line in content.split('\n'):
        rows.append(line.split(','))
        for index,cell_content in enumerate(rows[-1]):
            if cell_content.startswith('"') or cell_content.endswith('"'):
                rows[-1][index] = cell_content[1:-1]
    joined_rows = []
    for row in rows:
        joined_rows.append(",".join(row))
    return "\n".join(joined_rows)
