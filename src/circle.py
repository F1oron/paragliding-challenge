# coding: utf8

import math


def number2radius(number):
    """
    convert degree into radius
    Keyword arguments:
    number -- degree
    return radius
    """
    return number * math.pi / 180


def number2degree(number):
    """
    convert radius into degree
    Keyword arguments:
    number -- radius
    return degree
    """
    return number * 180 / math.pi


def draw_circle(radius_in_meters, center_point, steps=15):
    """
    get a circle shape polygon based on centerPoint and radius

    Keyword arguments:
    point1  -- point one geojson object
    point2  -- point two geojson object

    if(point inside multipoly) return true else false
    """
    steps = steps if steps > 15 else 15
    center = center_point
    dist = (radius_in_meters / 1000) / 6371
    # convert meters to radiant
    rad_center = [number2radius(center[0]), number2radius(center[1])]
    # 15 sided circle
    poly = []
    for step in range(0, steps):
        brng = 2 * math.pi * step / steps
        lat = math.asin(math.sin(rad_center[0]) * math.cos(dist) +
                        math.cos(rad_center[0]) * math.sin(dist) * math.cos(brng))
        lng = rad_center[1] + math.atan2(math.sin(brng) * math.sin(dist)
                                         * math.cos(rad_center[0]), math.cos(dist) - math.sin(rad_center[0]) * math.sin(lat))
        poly.append({"longitude": float('{:.13f}'.format(number2degree(lng))), "latitude": float('{:.13f}'.format(number2degree(lat)))})  # Limit size to 13 digit after dot, sometimes there is difference between windows and linux results
    return poly


if __name__ == '__main__':
    pt_center = [1.842460, 45.471114]
    print(draw_circle(300, pt_center, 50))
