# coding: utf8
import csv
import logging

import Output
import Task
import csvutils
from TurnpointFileParser import TurnpointFileParser

EXPORT_ONE_FILE = 0
EXPORT_ONE_FILE_PER_TASK_ELEMENT_TYPE = 1


class Convert:
    def __init__(self, waypoint_file, ethercalc_url=None, output_strategy=False, display_task_element_as_circle=False):
        self.waypoint_file = waypoint_file
        self.ethercalc_url = ethercalc_url

        file_segregation = EXPORT_ONE_FILE_PER_TASK_ELEMENT_TYPE if output_strategy else EXPORT_ONE_FILE
        self.output_files = self.extract_dictionnary_of_output_file_path(waypoint_file, file_segregation)

        self.task = None
        if display_task_element_as_circle:
            self.task_element_display_type = Output.USE_TURNPOINT_PERIPHERY
        else:
            self.task_element_display_type = Output.USE_TURNPOINT_CENTER

    @staticmethod
    def extract_output_file_path(input_file, filename_suffix=""):
        if input_file[:input_file.rfind('.')]:
            return '{}{}.geojson'.format(input_file[:input_file.rfind('.')], filename_suffix)
        return None

    def extract_dictionnary_of_output_file_path(self, input_file, output_strategy):
        if output_strategy == EXPORT_ONE_FILE:
            return [{"filepath": self.extract_output_file_path(input_file),
                     "taskElementToUse": Task.TAKEOFF | Task.LANDING | Task.TURNPOINT}]
        else:
            return [
                {"filepath": self.extract_output_file_path(input_file, "_takeoff"), "taskElementToUse": Task.TAKEOFF},
                {"filepath": self.extract_output_file_path(input_file, "_landing"), "taskElementToUse": Task.LANDING},
                {"filepath": self.extract_output_file_path(input_file, "_turnpoint"),
                 "taskElementToUse": Task.TURNPOINT}]

    def convert_turnpoints(self):
        if self.waypoint_file[self.waypoint_file.rfind('.'):] != ".csv":
            raise RuntimeError(f"File {self.waypoint_file} should be a CSV file")
        if self.ethercalc_url:
            csvutils.download_csv_from_ethercalc_and_save_it(self.ethercalc_url, self.waypoint_file)
        self.task = TurnpointFileParser(self.waypoint_file).parse_file() \
            .sort_point_by_category() \
            .provide_index_to_point() \
            .add_score_to_empty_turnpoints()
        self.write_output_file()

    def build_object(self, task_element_to_include):
        return {"turnpoints": self.task.build_object(task_element_to_include)}

    def write_output_file(self):
        logging.info(f"Waipoint file path : {self.waypoint_file}")
        wpt_file_name = f"{self.waypoint_file[:self.waypoint_file.rfind('.')]}.wpt"
        Output.output_factory(wpt_file_name).write_waypoint(
            self.task.build_object(Task.TAKEOFF | Task.LANDING | Task.TURNPOINT))
        for output_file in self.output_files:
            export = Output.output_factory(output_file["filepath"])
            if export:
                export.write_waypoint(self.build_object(output_file["taskElementToUse"]),
                                      self.task_element_display_type)
            else:
                logging.warning(
                    "{} file have unknown extension, output file will not be write".format(output_file["filepath"]))

