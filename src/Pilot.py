# coding: utf-8
import logging

from Turnpoint import TaskPoint


class Pilot:
    def __init__(self, name):
        self.name = name
        self.score = 0
        self.valid_turnpoints = []

    def __eq__(self, other):
        return self.name == other.name and self.valid_turnpoints == other.valid_turnpoints

    def __repr__(self):
        s = "{}\n".format(self.name)
        s += "Valid turpoints :\n"
        for turnpoint in self.valid_turnpoints:
            s += "\t{}\n".format(turnpoint)
        return s

    def serialize_string(self, details: bool = False):
        if details:
            turnpoints_str = ""
            for turnpoint in self.valid_turnpoints:
                turnpoints_str += " {} /".format(str(turnpoint))
            return "{} | {} |{}".format(self.name, round(self.score, 2), turnpoints_str[:-2])
        else:
            return "{} | {} | {}".format(self.name, round(self.score, 2), self.count_turnpoints())

    def append_turnpoint(self, turnpoint, pilot_coefficient: float = 1):
        for existing_turnpoint in self.valid_turnpoints:
            if existing_turnpoint.valid_turnpoint == turnpoint:
                existing_turnpoint.coefficient = max(existing_turnpoint.coefficient, pilot_coefficient)
                logging.debug("Update pilot {} turnpoint {} with bonus {}".format(self.name,
                                                                                  existing_turnpoint.valid_turnpoint.label,
                                                                                  existing_turnpoint.coefficient))
                return
        turnpoint_information = TurnpointInformations(turnpoint, pilot_coefficient)
        logging.debug("Add pilot {} turnpoint {} with bonus {}".format(self.name,
                                                                       turnpoint.label,
                                                                       pilot_coefficient))
        self.valid_turnpoints.append(turnpoint_information)

    def compute_total_score(self):
        self.score = 0
        for turnpoint in self.valid_turnpoints:
            self.score += (turnpoint.valid_turnpoint.score * turnpoint.coefficient)
        return round(self.score, 2)

    def count_turnpoints(self):
        return len(self.valid_turnpoints)

    def build_object(self):
        return {'name': self.name, 'score': round(self.score, 2), 'turnpoints': list(map(lambda x: x.build_object(), self.valid_turnpoints))}


class Pilots:
    def __init__(self):
        self.pilots = []

    def __repr__(self):
        s = "Pilot list :\n"
        for pilot in self.pilots:
            s += repr(pilot)
        return s

    def add_pilot(self, name):
        find_pilot = self.find_pilot_by_name(name)
        if not find_pilot:
            self.pilots.append(Pilot(name))

    def find_pilot_by_name(self, name):
        for pilot in self.pilots:
            if pilot.name == name:
                return pilot
        return None

    def get_or_create_pilot_by_name(self, pilot_name):
        pilot = self.find_pilot_by_name(pilot_name)
        if not pilot:
            pilot = Pilot(pilot_name)
            self.pilots.append(pilot)
        return pilot

    def compute_total_scores(self):
        score_list = []
        for pilot in self.pilots:
            score_list.append((pilot.name, pilot.compute_total_score()))
        score_list.sort(key=lambda y: y[1], reverse=True)
        self.sort_pilot_by_score()
        return score_list

    def sort_pilot_by_score(self):
        self.pilots.sort(key=lambda y: y.score, reverse=True)
        return self

    def serialize_string(self, details: bool = False):
        results = ""
        counter = 1
        for pilot in self.sort_pilot_by_score().pilots:
            results += "{} | {}\n".format(counter, pilot.serialize_string(details))
            counter += 1
        return results

    def build_object(self):
        return list(map(lambda x: x.build_object(), self.pilots))


class TurnpointInformations:
    def __init__(self, turnpoint: TaskPoint, coefficient: float):
        self.valid_turnpoint = turnpoint
        self.coefficient = coefficient

    def __eq__(self, other):
        return self.valid_turnpoint == other.valid_turnpoint and self.coefficient == other.coefficient

    def __repr__(self):
        return f'TurnpointInformation({self.valid_turnpoint}, {self.coefficient})'

    def __str__(self):
        return f'{self.valid_turnpoint.label} ({round(self.coefficient * self.valid_turnpoint.score, 2)})'

    def build_object(self):
        return {'name': self.valid_turnpoint.label, 'score': round(self.coefficient * self.valid_turnpoint.score, 2), "bonus": round(self.coefficient, 2)}
