# coding: utf-8
import logging

from Turnpoint import TaskPoint
from Pilot import Pilot

class Club:
    def __init__(self, name):
        self.name = name
        self.score = 0
        self.valid_turnpoints = []

    def __eq__(self, other):
        return self.name == other.name and self.valid_turnpoints == other.valid_turnpoints

    def __repr__(self):
        s = "{}\n".format(self.name)
        s += "Valid turpoints :\n"
        for turnpoint in self.valid_turnpoints:
            s += "\t{}\n".format(turnpoint)
        return s

    def serialize_string(self, details: bool = False):
        if details:
            turnpoints_str = ""
            for turnpoint in self.valid_turnpoints:
                turnpoints_str += " {} /".format(str(turnpoint))
            return "{} | {} |{}".format(self.name, round(self.score, 2), turnpoints_str[:-2])
        else:
            return "{} | {} | {}".format(self.name, round(self.score, 2), self.count_turnpoints())

    def append_turnpoint(self, turnpoint, pilot_coefficient: float = 1, pilot: Pilot = None):
        for existing_turnpoint in self.valid_turnpoints:
            if existing_turnpoint.valid_turnpoint == turnpoint:
                current_coefficient = existing_turnpoint.coefficient
                existing_turnpoint.coefficient = max(existing_turnpoint.coefficient, pilot_coefficient)
                if (current_coefficient != existing_turnpoint.coefficient):
                    existing_turnpoint.pilot = pilot
                logging.debug("Update club {}, pilot {} turnpoint {} with bonus {}".format(self.name,
                                                                                  existing_turnpoint.pilot.name,
                                                                                  existing_turnpoint.valid_turnpoint.label,
                                                                                  existing_turnpoint.coefficient))
                return
        turnpoint_information = TurnpointInformations(turnpoint, pilot_coefficient, pilot)
        logging.debug("Add club {}, pilot {} turnpoint {} with bonus {}".format(self.name,
                                                                        pilot.name,
                                                                       turnpoint.label,
                                                                       pilot_coefficient))
        self.valid_turnpoints.append(turnpoint_information)

    def compute_total_score(self):
        self.score = 0
        for turnpoint in self.valid_turnpoints:
            self.score += (turnpoint.valid_turnpoint.score * turnpoint.coefficient)
        return round(self.score, 2)

    def count_turnpoints(self):
        return len(self.valid_turnpoints)

    def build_object(self):
        return {'name': self.name, 'score': round(self.score, 2), 'turnpoints': list(map(lambda x: x.build_object(), self.valid_turnpoints))}


class Clubs:
    def __init__(self):
        self.clubs = []

    def __repr__(self):
        s = "club list :\n"
        for club in self.clubs:
            s += repr(club)
        return s

    def add_club(self, name):
        find_club = self.find_club_by_name(name)
        if not find_club:
            self.clubs.append(Club(name))

    def find_club_by_name(self, name):
        for club in self.clubs:
            if club.name == name:
                return club
        return None

    def get_or_create_club_by_name(self, club_name):
        club = self.find_club_by_name(club_name)
        if not club:
            club = Club(club_name)
            self.clubs.append(club)
        return club

    def compute_total_scores(self):
        score_list = []
        for club in self.clubs:
            score_list.append((club.name, club.compute_total_score()))
        score_list.sort(key=lambda y: y[1], reverse=True)
        self.sort_club_by_score()
        return score_list

    def sort_club_by_score(self):
        self.clubs.sort(key=lambda y: y.score, reverse=True)
        return self

    def serialize_string(self, details: bool = False):
        results = ""
        counter = 1
        for club in self.sort_pilot_by_score().clubs:
            results += "{} | {}\n".format(counter, club.serialize_string(details))
            counter += 1
        return results

    def build_object(self):
        return list(map(lambda x: x.build_object(), self.clubs))


class TurnpointInformations:
    def __init__(self, turnpoint: TaskPoint, coefficient: float, pilot: Pilot):
        self.valid_turnpoint = turnpoint
        self.coefficient = coefficient
        self.pilot = pilot

    def __eq__(self, other):
        return self.valid_turnpoint == other.valid_turnpoint and self.coefficient == other.coefficient

    def __repr__(self):
        return f'TurnpointInformation({self.valid_turnpoint}, {self.coefficient})'

    def __str__(self):
        return f'{self.valid_turnpoint.label} ({round(self.coefficient * self.valid_turnpoint.score, 2)})'

    def build_object(self):
        return {'name': self.valid_turnpoint.label, 'score': round(self.coefficient * self.valid_turnpoint.score, 2), 'bonus': round(self.coefficient, 2), 'pilot': self.pilot.name}
