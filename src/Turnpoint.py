from geopy.distance import great_circle

import circle
from GpsPoint import GpsPoint
from enum import Enum


class PointType(Enum):
    UNKNOWN = 0
    LANDING = 1
    TAKEOFF = 2
    TURNPOINT = 3


class TaskPoint:
    def __init__(self, coordinates: GpsPoint, radius: int, label: str = "", description="", altitude=-1, score: float = None, index: int = -1, pointType: PointType = PointType.UNKNOWN):
        self.label = label
        self.coordinates = coordinates
        self.radius = radius
        self.score = score
        self.description = description
        self.altitude = altitude
        self.index = index
        self.pointType = pointType
        if (label == ""):
            self.build_label()

    def __repr__(self):
        return "{} {} {} {}m {}pts".format(self.label, self.radius, self.coordinates, self.radius, self.score)

    def __eq__(self, other_task_point):
        return self.label == other_task_point.label and self.coordinates == other_task_point.coordinates \
               and self.radius == other_task_point.radius and self.description == other_task_point.description \
               and self.altitude == other_task_point.altitude and self.score == other_task_point.score

    def is_contain_track_point(self, track_point):
        return self.coordinates.get_distance_to_another_point(track_point.coordinates) <= self.radius

    def get_distance_to_point_peripheral(self, track_point):
        return self.coordinates.get_distance_to_another_point(track_point.coordinates) - self.radius

    def get_distance_to_point(self, track_point):
        return self.coordinates.get_distance_to_another_point(track_point.coordinates)

    def set_index(self, index):
        self.index = index
        self.build_label()

    def build_label(self):
        if self.index != -1 and self.pointType != PointType.UNKNOWN and self.altitude != -1:
            typeDescription = ""
            if self.pointType == PointType.LANDING:
                typeDescription = "A"
            elif self.pointType == PointType.TAKEOFF:
                typeDescription = "D"
            elif self.pointType == PointType.TURNPOINT:
                typeDescription = "B"
            self.label = "{}{:02d}{:03d}".format(typeDescription, self.index, int(self.altitude/10))

    def add_score(self, score: float):
        self.score = score

    def is_score_empty(self):
        return self.score == None

    def build_object(self):
        score = self.score if self.score != None else 0
        return {"index": self.index, "label": self.label, "score": score, "radius": self.radius, "coordinates": {"center": self.coordinates.build_object(), "periphery": circle.draw_circle(self.radius, [self.coordinates.lat, self.coordinates.lon], int(self.radius / 6))}, "description": self.description, "altitude": self.altitude, "type": self.pointType}
