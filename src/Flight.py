### CLASSES
import datetime


class Track:
    def __init__(self, pilotName, date, gpsReference, trackPoints):
        self.pilotName = pilotName
        self.date = date
        self.gpsReference = gpsReference
        self.trackPoints = trackPoints

    def __repr__(self):
        return "%s %s %s %d points" % (self.pilotName, self.date.strftime("%d/%m/%Y"), self.gpsReference, len(self.trackPoints))

    def searchPointInTurnpoint(self, turnpoint, notBeforeTime=None, notAfterTime=None):
        idx = -1
        for trackPoint in self.trackPoints:
            idx += 1
            if(notAfterTime and trackPoint.time > notAfterTime):
                break

            if(notBeforeTime and trackPoint.time < notBeforeTime):
                continue

            # TOREM
            #from geopy.distance import great_circle
            #tmp = great_circle(turnpoint.coordinates.toDegree(), trackPoint.coordinates.toDegree()).m
            #print("%s %d %s %s" % (trackPoint.time, tmp, turnpoint.coordinates, trackPoint.coordinates))
            if(turnpoint.isContainTrackPoint(trackPoint)):
                return idx


        return None

    def is_turnpoint_cross_by_track(self, turnpoint):
        index = 0
        while index < len(self.trackPoints):
            distance = turnpoint.get_distance_to_point_peripheral(self.trackPoints[index])
            if distance < 0:
                return True
            elif distance > 50:
                temp_datetime = datetime.datetime(2020, 1, 1, self.trackPoints[index].time.hour,
                                                  self.trackPoints[index].time.minute,
                                                  self.trackPoints[index].time.second)
                time_to_accelerate_to_go_to_next_point_to_test = distance / convert_km_per_hour_to_meter_per_second(80)
                delta = datetime.timedelta(0, int(time_to_accelerate_to_go_to_next_point_to_test))
                new_datetime = temp_datetime + delta
                next_point_time = datetime.time(new_datetime.hour, new_datetime.minute, new_datetime.second)
                if next_point_time <= self.trackPoints[index].time:
                    # We pass midnight
                    return False
                index_research_next_point = index
                while self.trackPoints[index_research_next_point].time < next_point_time:
                    if index_research_next_point < len(self.trackPoints) - 1:
                        index_research_next_point += 1
                    else:
                        return False
                index = index_research_next_point
            else:
                index += 1
        # for trackPoint in self.trackPoints:
        #     # TOREM
        #     #from geopy.distance import great_circle
        #     #tmp = great_circle(turnpoint.coordinates.toDegree(), trackPoint.coordinates.toDegree()).m
        #     #print("%s %d %s %s" % (trackPoint.time, tmp, turnpoint.coordinates, trackPoint.coordinates))
        #     if(turnpoint.is_contain_track_point(trackPoint)):
        #         return True

        return False

    def is_track_begin_in_turnpoint(self, turnpoint):
        for trackPoint in self.trackPoints[:9]:
            if turnpoint.is_contain_track_point(trackPoint):
                return True
        return False

    def is_track_end_in_turnpoint(self, turnpoint):
        for trackPoint in self.trackPoints[-10:]:
            if turnpoint.is_contain_track_point(trackPoint):
                return True
        return False

    def is_begin_and_end_of_flight_closed(self, maximum_distance):
        return self.trackPoints[0].coordinates.get_distance_to_another_point(self.trackPoints[-1].coordinates) <= maximum_distance


def convert_km_per_hour_to_meter_per_second(speed):
    return (speed * 1000) / 3600
