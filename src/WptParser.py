# coding: utf-8

import wpt
from Task import Task
from GpsPoint import GpsPoint
from Turnpoint import TaskPoint


class WptParser:
    def __init__(self, file_path):
        self.file_to_parse = file_path
        self.takeoff = []

    def parse(self):
        with open(self.file_to_parse, 'r') as file:
            wpt_object = wpt.loads(file.read())
            return self.extract_wpt_informations(wpt_object)

    def extract_wpt_informations(self, wpt_object):
        return Task(self.extract_element(wpt_object, 'D'))

    def extract_element(self, wpt, name_id_value):
        filtered_list = [element for element in wpt['waypoint'] if element['name'][0] == name_id_value]
        self.takeoff = list(map(self.generate_taskpoint_point, filtered_list))
        return self.takeoff

    @staticmethod
    def generate_taskpoint_point(point):
        return TaskPoint(label=point['name'],
                         coordinates=GpsPoint(point['latitude'], point['longitude']),
                         radius=point['proximity'],
                         description=point['description'],
                         altitude=point['altitude'])
