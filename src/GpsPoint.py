# coding: utf-8

from geopy.distance import great_circle


class GpsPoint:
    def __init__(self, lat, lon):
        if type(lat) == type(tuple) or type(lon) == type(tuple):
            raise Exception("GpsPoint constructor need float degree coordinates values")
        self.lat = lat
        self.lon = lon

    def __repr__(self):
        return "(%.5f,%.5f)" % (self.lat, self.lon)

    def __eq__(self, other_point):
        return self.lat == other_point.lat and self.lon == other_point.lon

    def to_degree(self):
        return self.lat, self.lon

    def get_distance_to_another_point(self, other_point):
        return great_circle(self.to_degree(), other_point.to_degree()).m

    def build_object(self):
        return {"longitude": self.lon, "latitude": self.lat}

    @staticmethod
    def from_degrees(lat, lon):
        return GpsPoint(lat, lon)

    @staticmethod
    def from_degrees_minutes(lat, lon):
        conv_latitude = convert_gps_degrees_minutes_to_degree(lat[0], lat[1], lat[2])
        conv_longitude = convert_gps_degrees_minutes_to_degree(lon[0], lon[1], lon[2])
        return GpsPoint(conv_latitude, conv_longitude)

    @staticmethod
    def from_degrees_minutes_seconds(lat, lon):
        conv_latitude = convert_gps_degrees_minutes_seconds_to_degree(lat[0], lat[1], lat[2], lat[3])
        conv_longitude = convert_gps_degrees_minutes_seconds_to_degree(lon[0], lon[1], lon[2], lon[3])
        return GpsPoint(conv_latitude, conv_longitude)


### FUNCTIONS
def convert_gps_degrees_minutes_to_degree(degrees, minutes, dir):
    if dir in ["W", "S"]:
        return -(degrees + float(minutes) / 60)
    if dir in ["N", "E"]:
        return degrees + float(minutes) / 60
    else:
        raise Exception("GPS cardinal point is not valid")


def convert_gps_degrees_minutes_seconds_to_degree(degrees, minutes, seconds, dir):
    if dir in ["W", "S"]:
        return -(degrees + minutes / 60. + seconds / 3600.)
    if dir in ["N", "E"]:
        return degrees + minutes / 60. + seconds / 3600.
    else:
        raise Exception("GPS cardinal point is not valid")
