# coding: utf-8
import csv
import json
from datetime import datetime
import yaml

USE_TURNPOINT_CENTER = 0
USE_TURNPOINT_PERIPHERY = 1


class TextType:
    def __init__(self, file_path):
        self.file_path = file_path

    def write_score(self, object):
        object['pilots'] = list(filter(lambda x: len(x['turnpoints']) != 0, object['pilots']))
        data = 'Résultats du challenge cross organisé du {} au {}\n\n'.format(object['dates']['min'],
                                                                              object['dates']['max'])
        data += 'Le challenge est constitué de {} balises à franchir, les coefficients multiplicateurs suivants sont appliqués :\n'.format(
            len(object['turnpoints']))
        for wing in object['bonus']['wingCategories']:
            data += 'Voile {} : {}\n'.format(wing['category'], wing['coefficient'])

        data += 'Parcours bouclé : {}\n'.format(object['bonus']['loop'])

        data += '\nLe classement est le suivant :\n'

        max_column_size = [len('Place'), len('Pilote'), len('Nombre de points'), len('Nombre de balises validées')]
        for pilot in object['pilots']:
            max_column_size[1] = max(max_column_size[1], len(pilot['name']))
            max_column_size[2] = max(max_column_size[2], len(str(pilot['score'])))
            max_column_size[3] = max(max_column_size[3], len(str(len(pilot['turnpoints']))))

        data += '{} | {} | {} | {}\n'.format('Place'.ljust(max_column_size[0]), 'Pilote'.ljust(max_column_size[1]),
                                             'Nombre de points'.ljust(max_column_size[2]),
                                             'Nombre de balises validées'.ljust(max_column_size[3]))
        data += "{}|{}|{}|{}\n".format(''.ljust(max_column_size[0] + 1, '-'), ''.ljust(max_column_size[1] + 2, '-'),
                                       ''.ljust(max_column_size[2] + 2, '-'), ''.ljust(max_column_size[3] + 1, '-'))

        for pilot_classement, pilot in enumerate(object['pilots'], start=1):
            data += '{} | {} | {} | {}\n'.format(str(pilot_classement).ljust(max_column_size[0]),
                                                 pilot['name'].ljust(max_column_size[1]),
                                                 str(pilot['score']).ljust(max_column_size[2]),
                                                 str(len(pilot['turnpoints'])).ljust(max_column_size[3]))

        data += '\nFichier généré le {}'.format(datetime.today().strftime("%d/%m/%Y à %X"))

        with open(self.file_path, 'w', newline='', encoding='utf-8') as txtFile:
            txtFile.write(data)

    def write_waypoint(self, object, turnpoint_format=USE_TURNPOINT_CENTER):
        print("Txt output need to be implemented")


class CsvType:
    def __init__(self, file_path):
        self.file_path = file_path

    def write_score(self, object):
        object['pilots'] = list(filter(lambda x: len(x['turnpoints']) != 0, object['pilots']))
        with open(self.file_path, 'w', newline='', encoding='utf-8') as csvfile:
            fieldnames = ['Classement', 'Pilote', 'Points', 'NbBalises']
            turnpoint_list = list(map(lambda x: x['label'], object['turnpoints']))
            fieldnames += turnpoint_list
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            for pilot_classement, pilot in enumerate(object['pilots'], start=1):
                line = {'Classement': pilot_classement, 'Pilote': pilot['name'], 'Points': pilot['score'],
                        'NbBalises': len(pilot['turnpoints'])}
                for turnpoint in pilot['turnpoints']:
                    line[turnpoint['name']] = turnpoint['score']
                writer.writerow(line)

    def write_waypoint(self, object, turnpoint_format=USE_TURNPOINT_CENTER):
        print("csv output need to be implemented")


class JsonType:
    def __init__(self, file_path):
        self.file_path = file_path

    def write_score(self, object):
        print("Json output need to be implemented")

    def write_waypoint(self, object, turnpoint_format=USE_TURNPOINT_CENTER):
        print("Json output need to be implemented")


class GeojsonType:
    def __init__(self, file_path):
        self.file_path = file_path

    def write_score(self, object):
        print("Geojson output need to be implemented")

    def write_waypoint(self, object, turnpoint_format=USE_TURNPOINT_CENTER):
        geojson_data = {"type": "FeatureCollection", "date": f'{datetime.today().strftime("%d/%m/%Y - %X")}'}
        features = []

        for turnpoint in object['turnpoints']:
            feature = {"type": "Feature",
                       "properties": {"name": turnpoint["label"], "description": turnpoint["description"],
                                      "altitude": turnpoint["altitude"], "proximity": turnpoint["radius"]}}
            if turnpoint["score"] != 0:
                feature["properties"]["score"] = turnpoint["score"]
            if turnpoint_format == USE_TURNPOINT_CENTER:
                feature["geometry"] = {"type": "Point", "coordinates": [turnpoint["coordinates"]["center"]["longitude"],
                                                                        turnpoint["coordinates"]["center"]["latitude"]]}
            else:
                feature["geometry"] = {"type": "Polygon", "coordinates": [
                    list(map(lambda x: [x["longitude"], x["latitude"]], turnpoint["coordinates"]["periphery"]))]}
            features.append(feature)
        geojson_data["features"] = features
        serialized_data = json.dumps(geojson_data)
        with open(self.file_path, 'w', newline='', encoding='utf-8') as geojsonfile:
            geojsonfile.write(serialized_data)


class WptType:
    def __init__(self, file_path):
        self.file_path = file_path

    def write_score(self, object):
        print("Wpt file do not implement score storage")

    def write_waypoint(self, object, turnpoint_format=USE_TURNPOINT_CENTER):
        file_content = "OziExplorer Waypoint File Version 1.1\n" \
                       "WGS 84\n" \
                       "Reserved 2\n" \
                       "Reserved 3\n"
        for turnpoint in object:
            file_content += "{:02d},{},{},{},,0,1,3,0,65535,{},0,0,{},{},6,0,17\n".format(
                turnpoint["index"],
                turnpoint["label"],
                turnpoint["coordinates"]["center"]["latitude"],
                turnpoint["coordinates"]["center"]["longitude"],
                turnpoint["description"],
                turnpoint["radius"],
                int(turnpoint["altitude"] * 3.2808)
            )
        with open(self.file_path, 'w', newline='', encoding='utf-8') as wpt_file:
            wpt_file.write(file_content)

class YamlType:
    def __init__(self, file_path):
        self.file_path = file_path

    def write_score(self, object):
        yaml_object = {"file": {"date": f'{datetime.today().strftime("%d/%m/%Y")}', "hour": f'{datetime.today().strftime("%X")}', "timestamp": f'{datetime.today()}'}, "rules": {"dates": {"start": object["dates"]["min"], "end": object["dates"]["max"]}, "loopBonus": object["bonus"]["loop"]}}
        wings = []
        for wing in object["bonus"]["wingCategories"]:
            wings.append({"wing": wing["category"], "bonus": wing["coefficient"]})
        yaml_object["rules"]["wingBonus"] = wings
        
        pilots = []
        for position, pilot in enumerate(object['pilots'], start=1):
            if (len(pilot["turnpoints"])):
                pilot_object = {"pilot": pilot["name"], "score": pilot["score"], "nbTurnpoints": len(pilot["turnpoints"]), "position": position}
                turnpoints = []
                for turnpoint in pilot["turnpoints"]:
                    turnpoints.append({"label": turnpoint["name"], "score": turnpoint["score"], "bonus": turnpoint["bonus"]})
                pilot_object["turnpoints"] = turnpoints
                pilots.append(pilot_object)

        yaml_object["scores"] = pilots

        clubs = []
        for position, club in enumerate(object['clubs'], start=1):
            if (len(club["turnpoints"])):
                club_object = {"club": club["name"], "score": club["score"], "nbTurnpoints": len(club["turnpoints"]), "position": position}
                turnpoints = []
                for turnpoint in club["turnpoints"]:
                    turnpoints.append({"label": turnpoint["name"], "score": turnpoint["score"], "bonus": turnpoint["bonus"], "pilot": turnpoint["pilot"]})
                club_object["turnpoints"] = turnpoints
                clubs.append(club_object)

        yaml_object["clubs"] = clubs

        with open(self.file_path, 'w', newline='', encoding='utf-8') as yml_file:
            yml_file.write(yaml.dump(yaml_object))

    def write_waypoint(self, object, turnpoint_format=USE_TURNPOINT_CENTER):
        print("Yaml fil do not implement waypoint definition")


def output_factory(file_path):
    output_type = {
        ".txt": TextType,
        ".csv": CsvType,
        ".json": JsonType,
        ".geojson": GeojsonType,
        ".wpt": WptType,
        ".yml": YamlType
    }
    if file_path[file_path.rfind('.'):] in output_type:
        return output_type[file_path[file_path.rfind('.'):]](file_path)
    else:
        return None
