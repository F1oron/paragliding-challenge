# coding: utf-8

from Turnpoint import TaskPoint, PointType
from Task import Task
from GpsPoint import GpsPoint
import csv


class TurnpointFileParser:
    def __init__(self, file_path):
        self.file_path = file_path

    def parse_file(self):
        with open(self.file_path, 'r') as file:
            file_content = csv.reader(file)
            return self.parse_content(file_content)

    def parse_content(self, content):
        description_line_passed = False
        turnpoint_list = []
        for line in content:
            if not description_line_passed:
                description_line_passed = True
            else:
                if line[2] == "takeoff":
                    point_type = PointType.TAKEOFF
                elif line[2] == "landing":
                    point_type = PointType.LANDING
                elif line[2] == "turnpoint":
                    point_type = PointType.TURNPOINT
                else:
                    point_type = PointType.UNKNOWN
                if line[6] == "":
                    score = None
                else:
                    score = float(line[6])
                turnpoint_list.append(TaskPoint(coordinates=GpsPoint(float(line[0]), float(line[1])),
                                                pointType=point_type,
                                                description=line[3],
                                                altitude=int(line[4]),
                                                radius=int(line[5]),
                                                score=score))
        return Task(turnpoint_list)
