# coding: utf-8
import datetime
import re
import logging

import Task
from Output import output_factory
from Parser.TrackParser.IGCParser import IGCParser
import Pilot, Club
from Turnpoint import PointType
from TurnpointFileParser import TurnpointFileParser
from WptParser import WptParser
import os
import csv

from bonus import Bonus

RE_FLIGHT_FILE_FORMAT = re.compile("^([0-9]{4})-([0-9]{2})-([0-9]{2})_([a-zA-ZÀ-ú\-]+)_([a-zA-ZÀ-ú\-]+)_([A-D]{1}|[C]{3})_([a-zA-Z0-9]+)[a-zA-Z0-9\-]*.igc$")
RE_DATE_FORMAT = re.compile("^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$")


class Challenge:
    def __init__(self, task_file_path, flight_file_directory, rules_file_path, output_file):
        self.task_file_path = task_file_path
        self.flight_file_directory = flight_file_directory
        self.rules_file_path = rules_file_path
        self.task = None
        self.pilot_list = Pilot.Pilots()
        self.club_list = Club.Clubs()
        self.challenge_bonus = None
        self.output_file = output_file
        self.startDate = datetime.datetime(1970, 1, 1)
        self.endDate = datetime.datetime.now()
        self.challenge_object = None

    def compute_scores(self):
        self.task = TurnpointFileParser(self.task_file_path).parse_file() \
            .sort_point_by_category() \
            .provide_index_to_point() \
            .add_score_to_empty_turnpoints()

        self.extract_bonus().parse_flight_files().write_output_file()

    def write_output_file(self):
        export = output_factory(self.output_file)
        if export:
            export.write_score(self.build_object())
        else:
            logging.warning("{} file have unknown extension, output file will not be write".format(self.output_file))

    def parse_flight_files(self):
        directory = os.scandir(self.flight_file_directory)
        directory_object = [
            {
                'name': file.name,
                'path': file.path,
                'is_file': file.is_file()
            }
            for file in directory
        ]
        directory_object.sort(key=lambda x: x['name'])

        for flight_file in directory_object:
            self.parse_flight_file(flight_file)
        return self

    def parse_flight_file(self, flight_filename):
        if not flight_filename['is_file']:
            return None

        match = RE_FLIGHT_FILE_FORMAT.match(flight_filename['name'])
        if not match:
            logging.warning('Flight file {} will not be parsed'.format(flight_filename['name']))
            return None
        else:
            flight_date = datetime.datetime(int(match.group(1)), int(match.group(2)), int(match.group(3)))

            if self.verify_flight_date(flight_date):
                logging.info("File {} out of challenge dates, not parse".format(flight_filename['name']))
            else:
                logging.info("Parse {} file".format(flight_filename['name']))
                igc_parser = IGCParser(flight_filename['path'])
                flight = igc_parser.parse()

                if flight:
                    pilot = self.pilot_list.get_or_create_pilot_by_name("{} {}".format(match.group(4), match.group(5)))
                    club = self.club_list.get_or_create_club_by_name(f'{match.group(7)}')
                    current_flight_valid_turnpoints = []
                    is_flight_make_loop = self.is_flight_make_a_loop(flight)

                    for turnpoint in self.task.filter_point_type(PointType.TURNPOINT):
                        if flight.is_turnpoint_cross_by_track(turnpoint):
                            if turnpoint not in current_flight_valid_turnpoints:
                                logging.debug("Bonus infos are wing : {}, flight loop : {}".format(match.group(6), is_flight_make_loop))
                                pilot.append_turnpoint(turnpoint, self.challenge_bonus.compute_bonus(match.group(6),
                                                                                                     is_flight_make_loop))
                                club.append_turnpoint(turnpoint, self.challenge_bonus.compute_bonus(match.group(6), is_flight_make_loop), pilot)
        return self

    def verify_flight_date(self, flight_date):
        return flight_date < self.startDate or flight_date > self.endDate

    def is_flight_make_a_loop(self, flight):
        flight_start_on_takeoff = False
        flight_end_on_take_off = False
        flight_end_on_landing = False
        for takeoff in self.task.filter_point_type(PointType.TAKEOFF):
            if flight.is_track_begin_in_turnpoint(takeoff):
                flight_start_on_takeoff = True
            if flight.is_track_end_in_turnpoint(takeoff):
                flight_end_on_take_off = True
        for landing in self.task.filter_point_type(PointType.LANDING):
            if flight.is_track_end_in_turnpoint(landing):
                flight_end_on_landing = True
        is_flight_make_loop = flight_start_on_takeoff and (flight_end_on_landing or flight_end_on_take_off) and flight.is_begin_and_end_of_flight_closed(5000)
        if is_flight_make_loop:
            logging.debug("Flight makes a loop")
        return is_flight_make_loop

    def extract_bonus(self):
        categories = {}
        loop = 1
        with open(self.rules_file_path, 'r') as file:
            csv_file = csv.reader(file, delimiter=';')
            for line in csv_file:
                if line[0] == 'WingBonus':
                    categories[line[1]] = float(line[2].replace(',', '.'))
                if line[0] == 'LoopBonus':
                    if line[1] == 'Yes':
                        loop = float(line[2].replace(',', '.'))
                if line[0] == 'Date':
                    match = RE_DATE_FORMAT.match(line[2])
                    if not match:
                        logging.error("Input date error")
                    else:
                        date = datetime.datetime(int(match.group(3)), int(match.group(2)), int(match.group(1)))
                        if line[1] == 'Start':
                            self.startDate = date
                        if line[1] == 'End':
                            self.endDate = date

        self.challenge_bonus = Bonus(categories, loop)
        return self

    def build_object(self):
        self.pilot_list.compute_total_scores()
        self.club_list.compute_total_scores()
        self.challenge_object = {"dates": {"min": self.startDate.strftime('%d/%m/%Y'), "max": self.endDate.strftime('%d/%m/%Y')}, "turnpoints": self.task.build_object(), "bonus": self.challenge_bonus.build_object(), "pilots": self.pilot_list.build_object(), "clubs": self.club_list.build_object()}
        return self.challenge_object
