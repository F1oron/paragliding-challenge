# coding: utf-8

class Bonus:
    def __init__(self, wing_categories=None, fly_loop=1):
        self.fly_loop = fly_loop
        self.wing_categories = {'A': 1, 'B': 1, 'C': 1, 'D': 1, 'CCC': 1}
        if wing_categories:
            for key, value in wing_categories.items():
                if key in self.wing_categories:
                    self.wing_categories[key] = value

    def compute_bonus(self, wing_category, does_fly_loop):
        if (wing_category not in self.wing_categories) or (not type(does_fly_loop) is bool):
            raise RuntimeError("Some bonus information is not valid")
        bonus = self.wing_categories[wing_category]
        if does_fly_loop:
            bonus *= self.fly_loop
        return round(bonus, 2)

    def build_object(self):
        wing_categories_tab = []
        for key, value in self.wing_categories.items():
            wing_categories_tab.append({'category': key, 'coefficient': value})
        return {'wingCategories': wing_categories_tab, 'loop': self.fly_loop}
