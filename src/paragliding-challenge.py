# coding: utf-8
import argparse
import logging
import shutil
import time

from Archive import ArchiveFactory
from Challenge import Challenge
from Convert import Convert, EXPORT_ONE_FILE_PER_TASK_ELEMENT_TYPE, EXPORT_ONE_FILE


def compute_score(args):
    start_time = time.time()
    if args.verbose:
        logging.basicConfig(level=logging.INFO)
    logging.info('Started')

    challenge_directory = None
    task_file = None
    track_directory = None
    challenge_rules = None

    if args.task:
        task_file = args.task
    if args.tracks:
        track_directory = args.tracks
    if args.rules:
        challenge_rules = args.rules
    if args.archive:
        logging.info('Extract archive')
        archive = ArchiveFactory(args.archive)
        challenge_directory = archive.extract()
        task_file = f'{challenge_directory}/challenge/balises.csv'
        track_directory = f'{challenge_directory}/flights/'
        challenge_rules = f'{challenge_directory}/challenge/rules.csv'

    challenge = Challenge(task_file, track_directory, challenge_rules, args.out)
    challenge.compute_scores()

    if challenge_directory:
        shutil.rmtree(challenge_directory)
    duration = time.time() - start_time
    logging.info(f'Duration {duration} seconds')
    exit(0)


def convert(args):
    start_time = time.time()
    if args.verbose:
        logging.basicConfig(level=logging.INFO)
    logging.info('Started')

    ethercalc_url = None
    if args.ethercalc:
        ethercalc_url = args.ethercalc

    conversion = Convert(args.task, ethercalc_url, args.segregate, args.circle)
    conversion.convert_turnpoints()

    duration = time.time() - start_time
    logging.info(f'Duration {duration} seconds')
    exit(0)


def parse_arguments_and_run_app():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(help='sub-command help')

    parser_score = subparsers.add_parser('compute_score', help='Compute challenge score')

    parser_score.add_argument("-a", "--archive",
                              type=str,
                              required=False,
                              help="Path to archive which contains all challenge files")

    parser_score.add_argument("--tracks",
                              type=str,
                              required=False,
                              help="Path to tracks directory (if archive isn't used)")

    parser_score.add_argument("--task",
                              type=str,
                              required=False,
                              help="Path to task file. CSV file containing waipoints and scores (if archive isn't used)")

    parser_score.add_argument("--rules",
                              type=str,
                              required=False,
                              help="Path to the file containing rules of the challenge (if archive isn't used)")

    parser_score.add_argument("-o", "--out",
                              type=str,
                              required=False,
                              help="Path to output file (accept .txt, .csv and .yml extention")

    parser_score.add_argument("-v", "--verbose",
                              action="store_true",
                              help="Print script working informations")

    parser_score.set_defaults(func=compute_score)

    parser_convert = subparsers.add_parser('convert',
                                           help='Convert waypoint files')

    parser_convert.add_argument("-t", "--task",
                                type=str,
                                required=True,
                                help="CSV file containing waipoints and scores to convert")

    parser_convert.add_argument("-c", "--circle",
                                action="store_true",
                                help="Display waipoints as circle instead of points")

    parser_convert.add_argument("--segregate",
                                action="store_true",
                                help="Output will be divide in three files, one per waypoint type (takeoff, landing, turnpoint)")

    parser_convert.add_argument("-e", "--ethercalc",
                                required=False,
                                type=str,
                                help="URL of a Ethercalc page to export CSV file")

    parser_convert.add_argument("-v", "--verbose",
                              action="store_true",
                              help="Print script working informations")

    parser_convert.set_defaults(func=convert)

    arguments = parser.parse_args()

    arguments.func(arguments)


if __name__ == "__main__":
    parse_arguments_and_run_app()
