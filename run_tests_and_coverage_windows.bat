@echo off
set PYTHONPATH="%cd%\test;%cd%\src;%cd%\src\Parser;%cd%\src\Parser\TrackParser"
cd test
coverage run -m unittest
coverage report -m
cd ..
pause