from unittest import TestCase

from GpsPoint import GpsPoint
from Pilot import Pilot, TurnpointInformations, Pilots
from Turnpoint import TaskPoint


class TestPilot(TestCase):
    def test_append_turnpoint(self):
        pilot = Pilot("Karadoc")
        turnpoint1 = TaskPoint(GpsPoint(45.2658, 1.25698), label="B01658", description="", radius=300, score=10)
        pilot.append_turnpoint(turnpoint1, 1)
        turnpoint2 = TaskPoint(GpsPoint(45.2558, 1.27698), label="B01659", description="", radius=350, score=8.45)
        pilot.append_turnpoint(turnpoint2, 1.5)
        self.assertListEqual([TurnpointInformations(turnpoint1, 1), TurnpointInformations(turnpoint2, 1.5)], pilot.valid_turnpoints)
        self.assertEqual(2, pilot.count_turnpoints())
        self.assertEqual(22.67, pilot.compute_total_score())
        self.assertEqual("Karadoc | 22.67 | 2", pilot.serialize_string())
        self.assertEqual("Karadoc | 22.67 | B01658 (10) / B01659 (12.67)", pilot.serialize_string(True))
        self.assertEqual({"name": "Karadoc", "score": 22.67, "turnpoints": [{"name": "B01658", "score": 10, "bonus": 1}, {"name": "B01659", "score": 12.67, "bonus": 1.5}]}, pilot.build_object())

    def test_append_turnpoint_should_not_add_turnpoint_with_existing_id(self):
        pilot = Pilot("Perceval")
        turnpoint = TaskPoint(GpsPoint(45.2658, 1.25698), label="B01658", description="", radius=300, score=10)
        pilot.append_turnpoint(turnpoint, 3)
        pilot.append_turnpoint(turnpoint, 3)
        self.assertListEqual([TurnpointInformations(turnpoint, 3)], pilot.valid_turnpoints)
        self.assertEqual(1, pilot.count_turnpoints())

    def test_append_turnpoint_should_edit_turnpoint_coefficient_when_two_same_turnpoint_are_append_and_second_has_higher_coeff(self):
        pilot = Pilot("Perceval")
        turnpoint = TaskPoint(GpsPoint(45.2658, 1.25698), label="B01658", description="", radius=300, score=10)
        pilot.append_turnpoint(turnpoint, 2.3)
        pilot.append_turnpoint(turnpoint, 3)
        self.assertListEqual([TurnpointInformations(turnpoint, 3)], pilot.valid_turnpoints)
        self.assertEqual(30, pilot.compute_total_score())
        self.assertEqual(1, pilot.count_turnpoints())
        self.assertEqual("Perceval | 30 | 1", pilot.serialize_string())
        self.assertEqual("Perceval | 30 | B01658 (30)", pilot.serialize_string(True))
        self.assertEqual({"name": "Perceval", "score": 30, "turnpoints": [{"name": "B01658", "score": 30, "bonus": 3}]}, pilot.build_object())

    def test_append_turnpoint_should_not_edit_turnpoint_coefficient_when_two_same_turnpoint_are_append_and_second_has_lower_coeff(self):
        pilot = Pilot("Perceval")
        turnpoint = TaskPoint(GpsPoint(45.2658, 1.25698), label="B01658", description="", radius=300, score=10)
        pilot.append_turnpoint(turnpoint, 2.2)
        pilot.append_turnpoint(turnpoint, 1.2)
        self.assertListEqual([TurnpointInformations(turnpoint, 2.2)], pilot.valid_turnpoints)
        self.assertEqual(22, pilot.compute_total_score())
        self.assertEqual(1, pilot.count_turnpoints())
        self.assertEqual("Perceval | 22.0 | 1", pilot.serialize_string())
        self.assertEqual("Perceval | 22.0 | B01658 (22.0)", pilot.serialize_string(True))
        self.assertEqual({"name": "Perceval", "score": 22.0, "turnpoints": [{"name": "B01658", "score": 22, "bonus": 2.2}]}, pilot.build_object())

    def test_append_turnpoint_should_add_two_turnpoints_to_Perceval(self):
        pilot = Pilot("Perceval")
        turnpoint = TaskPoint(GpsPoint(45.2658, 1.25698), label="B01658", description="", radius=300, score=10)
        pilot.append_turnpoint(turnpoint, 1)
        turnpoint = TaskPoint(GpsPoint(45.2558, 1.27698), label="B01659", description="", radius=350, score=8.45)
        pilot.append_turnpoint(turnpoint, 1.5)
        self.assertEqual(22.67, pilot.compute_total_score())
        self.assertEqual(2, pilot.count_turnpoints())
        self.assertEqual("Perceval | 22.67 | 2", pilot.serialize_string())
        self.assertEqual({"name": "Perceval", "score": 22.67, "turnpoints": [{"name": "B01658", "score": 10, "bonus": 1}, {"name": "B01659", "score": 12.67, "bonus": 1.5}]}, pilot.build_object())


class TestPilots(TestCase):
    def test_add_pilot(self):
        pilot_list = Pilots()
        pilot_list.add_pilot("Arthur")
        pilot_list.add_pilot("Perceval")
        pilot_list.add_pilot("Karadoc")
        self.assertListEqual([Pilot("Arthur"), Pilot("Perceval"), Pilot("Karadoc")], pilot_list.pilots)

    def test_add_pilot_should_not_add_pilot_with_same_name(self):
        pilot_list = Pilots()
        pilot_list.add_pilot("Arthur")
        pilot_list.add_pilot("Perceval")
        pilot_list.add_pilot("Karadoc")
        pilot_list.add_pilot("Arthur")
        pilot_list.add_pilot("Perceval")
        self.assertListEqual([Pilot("Arthur"), Pilot("Perceval"), Pilot("Karadoc")], pilot_list.pilots)

    def test_get_or_create_pilot_by_name(self):
        pilot_list = Pilots()
        self.assertEqual(pilot_list.get_or_create_pilot_by_name("Karadoc"), Pilot("Karadoc"))
        self.assertEqual(pilot_list.get_or_create_pilot_by_name("Perceval"), Pilot("Perceval"))
        self.assertEqual(pilot_list.get_or_create_pilot_by_name("Karadoc"), Pilot("Karadoc"))
        self.assertEqual(pilot_list.get_or_create_pilot_by_name("Arthur"), Pilot("Arthur"))
        self.assertEqual(pilot_list.get_or_create_pilot_by_name("Arthur"), Pilot("Arthur"))
        self.assertListEqual([Pilot("Karadoc"), Pilot("Perceval"), Pilot("Arthur")], pilot_list.pilots)

    def test_compute_and_sort_score_of_pilots(self):
        pilot_list = Pilots()
        self.assertEqual(Pilot("Karadoc"), pilot_list.get_or_create_pilot_by_name("Karadoc"))
        self.assertEqual(Pilot("Perceval"), pilot_list.get_or_create_pilot_by_name("Perceval"))
        perceval = pilot_list.get_or_create_pilot_by_name("Perceval")
        perceval.append_turnpoint(TaskPoint(label="B01658", coordinates=GpsPoint(45.2658, 1.25698), radius=300, score=5), 1.2)
        perceval.append_turnpoint(TaskPoint(label="B01659", coordinates=GpsPoint(45.2558, 1.27698), radius=350, score=7), 1.7)
        karadoc = pilot_list.get_or_create_pilot_by_name("Karadoc")
        karadoc.append_turnpoint(TaskPoint(label="B02519", coordinates=GpsPoint(44.2628, 1.63698), radius=200, score=6.54), 1.3)
        karadoc.append_turnpoint(TaskPoint(label="B03485", coordinates=GpsPoint(45.3597, 1.14536), radius=350, score=3.2), 1.4)
        scores = pilot_list.compute_total_scores()
        self.assertListEqual([("Perceval", 17.9), ("Karadoc", 12.98)], scores)
        self.assertEqual("1 | Perceval | 17.9 | 2\n2 | Karadoc | 12.98 | 2\n", pilot_list.serialize_string())
        self.assertEqual("1 | Perceval | 17.9 | B01658 (6.0) / B01659 (11.9)\n2 | Karadoc | 12.98 | B02519 (8.5) / B03485 (4.48)\n", pilot_list.serialize_string(True))
        self.assertEqual([{"name": "Perceval", "score": 17.9, "turnpoints": [{"name": "B01658", "score": 6.0, "bonus": 1.2}, {"name": "B01659", "score": 11.9, "bonus": 1.7}]}, {"name": "Karadoc", "score": 12.98, "turnpoints": [{"name": "B02519", "score": 8.5, "bonus": 1.3}, {"name": "B03485", "score": 4.48, "bonus": 1.4}]}], pilot_list.build_object())

    def test_compute_and_sort_score_of_pilots2(self):
        pilot_list = Pilots()
        self.assertEqual(Pilot("Perceval"), pilot_list.get_or_create_pilot_by_name("Perceval"))
        self.assertEqual(Pilot("Karadoc"), pilot_list.get_or_create_pilot_by_name("Karadoc"))
        perceval = pilot_list.get_or_create_pilot_by_name("Perceval")
        perceval.append_turnpoint(TaskPoint(label="B01658", coordinates=GpsPoint(45.2658, 1.25698), radius=300, score=5), 1.2)
        perceval.append_turnpoint(TaskPoint(label="B01659", coordinates=GpsPoint(45.2558, 1.27698), radius=350, score=7), 1.7)
        karadoc = pilot_list.get_or_create_pilot_by_name("Karadoc")
        karadoc.append_turnpoint(TaskPoint(label="B02519", coordinates=GpsPoint(44.2628, 1.63698), radius=200, score=6.54), 1.3)
        karadoc.append_turnpoint(TaskPoint(label="B03485", coordinates=GpsPoint(45.3597, 1.14536), radius=350, score=3.2), 1.4)
        scores = pilot_list.compute_total_scores()
        self.assertListEqual([("Perceval", 17.9), ("Karadoc", 12.98)], scores)
        self.assertEqual("1 | Perceval | 17.9 | 2\n2 | Karadoc | 12.98 | 2\n", pilot_list.serialize_string())
        self.assertEqual("1 | Perceval | 17.9 | B01658 (6.0) / B01659 (11.9)\n2 | Karadoc | 12.98 | B02519 (8.5) / B03485 (4.48)\n", pilot_list.serialize_string(True))
        self.assertEqual([{"name": "Perceval", "score": 17.9, "turnpoints": [{"name": "B01658", "score": 6.0, "bonus": 1.2}, {"name": "B01659", "score": 11.9, "bonus": 1.7}]}, {"name": "Karadoc", "score": 12.98, "turnpoints": [{"name": "B02519", "score": 8.5, "bonus": 1.3}, {"name": "B03485", "score": 4.48, "bonus": 1.4}]}], pilot_list.build_object())

