from unittest import TestCase
from Turnpoint import TaskPoint, PointType
from GpsPoint import GpsPoint


class TestTurnPoint(TestCase):
    def test_turnpoint_build_label_when_index_altitude_and_type_are_set(self):
        takeoff = TaskPoint(coordinates=GpsPoint(45.468135, 1.850644), radius=2000, description="Deco SO Fournaise",
                            altitude=2689, pointType=PointType.TAKEOFF)
        landing = TaskPoint(coordinates=GpsPoint(45.56715, 1.3004), radius=300, description="Atterro SO Fournaise",
                            altitude=621, pointType=PointType.LANDING)
        turnpoint = TaskPoint(coordinates=GpsPoint(45.76315, 1.3904), radius=300, description="Balise Lambda",
                              altitude=765, pointType=PointType.TURNPOINT)
        takeoff.set_index(0)
        landing.set_index(1)
        turnpoint.set_index(2)
        expected_takeoff = TaskPoint(coordinates=GpsPoint(45.468135, 1.850644), radius=2000,
                                     description="Deco SO Fournaise", altitude=2689, pointType=PointType.TAKEOFF,
                                     label="D00268")
        expected_landing = TaskPoint(coordinates=GpsPoint(45.56715, 1.3004), radius=300,
                                     description="Atterro SO Fournaise", altitude=621, pointType=PointType.LANDING,
                                     label="A01062")
        expected_turnpoint = TaskPoint(coordinates=GpsPoint(45.76315, 1.3904), radius=300, description="Balise Lambda",
                                       altitude=765, pointType=PointType.TURNPOINT, label="B02076")
        self.assertEqual(expected_takeoff, takeoff)
        self.assertEqual(expected_landing, landing)
        self.assertEqual(expected_turnpoint, turnpoint)

    def test_turnpoint_should_build_label_when_index_altitude_and_type_are_given_in_class_declaration(self):
        takeoff = TaskPoint(coordinates=GpsPoint(45.468135, 1.850644), radius=2000, description="Deco SO Fournaise",
                            altitude=2689, pointType=PointType.TAKEOFF, index=5)
        landing = TaskPoint(coordinates=GpsPoint(45.56715, 1.3004), radius=300, description="Atterro SO Fournaise",
                            altitude=621, pointType=PointType.LANDING, index=12)
        turnpoint = TaskPoint(coordinates=GpsPoint(45.76315, 1.3904), radius=300, description="Balise Lambda",
                              altitude=765, pointType=PointType.TURNPOINT, index=26)
        expected_takeoff = TaskPoint(coordinates=GpsPoint(45.468135, 1.850644), radius=2000,
                                     description="Deco SO Fournaise", altitude=2689, pointType=PointType.TAKEOFF,
                                     label="D05268")
        expected_landing = TaskPoint(coordinates=GpsPoint(45.56715, 1.3004), radius=300,
                                     description="Atterro SO Fournaise", altitude=621, pointType=PointType.LANDING,
                                     label="A12062")
        expected_turnpoint = TaskPoint(coordinates=GpsPoint(45.76315, 1.3904), radius=300, description="Balise Lambda",
                                       altitude=765, pointType=PointType.TURNPOINT, label="B26076")
        self.assertEqual(expected_takeoff, takeoff)
        self.assertEqual(expected_landing, landing)
        self.assertEqual(expected_turnpoint, turnpoint)

    def test_turnpoint_should_build_object(self):
        turnpoint = TaskPoint(coordinates=GpsPoint(45.76315, 1.3904), radius=30, description="Balise Lambda",
                              altitude=765, pointType=PointType.TURNPOINT, index=15, label="B15147", score=3.56)
        expected_object = {'index': 15,
                           'label': 'B15147',
                           'score': 3.56,
                           'radius': 30,
                           'coordinates': {
                               'center': {'latitude': 45.76315, 'longitude': 1.3904},
                               'periphery': [{'latitude': 45.7634197964818, 'longitude': 1.3904},
                                             {'latitude': 45.7633964712425, 'longitude': 1.3905573000339},
                                             {'latitude': 45.7633305287232, 'longitude': 1.3906874011232},
                                             {'latitude': 45.7632333711078, 'longitude': 1.3907678074927},
                                             {'latitude': 45.7631217979431, 'longitude': 1.3907846163366},
                                             {'latitude': 45.7630151012698, 'longitude': 1.3907349216169},
                                             {'latitude': 45.7629317298358, 'longitude': 1.3906273163028},
                                             {'latitude': 45.7628860991905, 'longitude': 1.3904804063695},
                                             {'latitude': 45.7628860991905, 'longitude': 1.3903195936305},
                                             {'latitude': 45.7629317298358, 'longitude': 1.3901726836972},
                                             {'latitude': 45.7630151012698, 'longitude': 1.3900650783831},
                                             {'latitude': 45.7631217979431, 'longitude': 1.3900153836634},
                                             {'latitude': 45.7632333711078, 'longitude': 1.3900321925073},
                                             {'latitude': 45.7633305287232, 'longitude': 1.3901125988768},
                                             {'latitude': 45.7633964712425, 'longitude': 1.3902426999661}]},
                           'description': 'Balise Lambda',
                           'altitude': 765,
                           'type': PointType.TURNPOINT}
        self.maxDiff = None
        self.assertEqual(expected_object, turnpoint.build_object())
