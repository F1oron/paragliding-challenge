from unittest import TestCase
from Task import Task
from GpsPoint import GpsPoint
from Turnpoint import TaskPoint, PointType
from TurnpointFileParser import TurnpointFileParser


class TestTurnpointFileParser(TestCase):
    def test_parse_content(self):
        expected_task = Task([
            TaskPoint(coordinates=GpsPoint(45.471114, 1.84246),
                      radius=300,
                      description="DECO SE SUC AU MAY",
                      altitude=896,
                      score=None,
                      pointType=PointType.TAKEOFF),
            TaskPoint(coordinates=GpsPoint(45.459985, 1.848082),
                      radius=200,
                      description="ATTERO FREYSSELINES",
                      altitude=623,
                      score=None,
                      pointType=PointType.LANDING),
            TaskPoint(coordinates=GpsPoint(45.559215, 1.814446),
                      radius=300,
                      description="PLAGE DES BARIOUSSES",
                      altitude=568,
                      score=6.98,
                      pointType=PointType.TURNPOINT)
        ])

        turnpoint_parser = TurnpointFileParser("test_data/csv_project_1.csv")
        self.assertEqual(expected_task, turnpoint_parser.parse_file())
