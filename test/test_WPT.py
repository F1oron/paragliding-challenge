from unittest import TestCase

import wpt


class Test(TestCase):
    def test_load_wpt_should_load_one_line_file_version_1_1(self):
        file_data = "OziExplorer Waypoint File Version 1.1\n" \
                    " WGS 84\n" \
                    " Reserved 2\n" \
                    " Reserved 3\n" \
                    " 1,D01089,45.471114,1.842460,,0,1,3,0,65535,Deco SE Suc au may,0,0,0,2923,6,0,17 "
        wpt_object = wpt.loads(file_data)

        expected_turnpoint = {"index": 1, "name": "D01089", "latitude": 45.471114, "longitude": 1.84246,
                              "symbol": 0, "status": 1, "mapDisplayFormat": 3, "foregroundColor": 0,
                              "backgroundColor": 65535, "description": "Deco SE Suc au may", "pointerDirection": 0,
                              "garminDisplayFormat": 0, "proximity": 0, "altitude": 2923, "fontSize": 6, "fontStyle": 0,
                              "symbolSize": 17}

        self.assertEqual(wpt_object['ozi_version'], "1.1", "Ozi version is wrong")
        self.assertEqual(wpt_object['geodesic_system'], "WGS 84", "Geodesic system is wrong")
        self.assertDictEqual(wpt_object['waypoint'][0], expected_turnpoint)

    def test_load_wpt_should_load_two_lines_file_version_1_1(self):
        file_data = "OziExplorer Waypoint File Version 1.1\n" \
                    " WGS 84\n" \
                    " Reserved 2\n" \
                    " Reserved 3\n" \
                    " 1,D01089,45.471114,1.842460,,0,1,3,0,65535,Deco SE Suc au may,0,0,0,2923,6,0,17 \n" \
                    " 16,A01062,45.459446,1.848441,,0,1,3,0,65535,Attero Freysselines,0,0,1000,2034,6,0,17"

        wpt_object = wpt.loads(file_data)

        expected_waypoint = [{"index": 1, "name": "D01089", "latitude": 45.471114, "longitude": 1.84246,
                              "symbol": 0, "status": 1, "mapDisplayFormat": 3, "foregroundColor": 0,
                              "backgroundColor": 65535, "description": "Deco SE Suc au may", "pointerDirection": 0,
                              "garminDisplayFormat": 0, "proximity": 0, "altitude": 2923, "fontSize": 6, "fontStyle": 0,
                              "symbolSize": 17},
                             {"index": 16, "name": "A01062", "latitude": 45.459446, "longitude": 1.848441,
                              "symbol": 0, "status": 1, "mapDisplayFormat": 3, "foregroundColor": 0,
                              "backgroundColor": 65535, "description": "Attero Freysselines", "pointerDirection": 0,
                              "garminDisplayFormat": 0, "proximity": 1000, "altitude": 2034, "fontSize": 6,
                              "fontStyle": 0,
                              "symbolSize": 17}
                             ]

        self.assertEqual(wpt_object['ozi_version'], "1.1", "Ozi version is wrong")
        self.assertEqual(wpt_object['geodesic_system'], "WGS 84", "Geodesic system is wrong")
        self.assertDictEqual(wpt_object['waypoint'][0], expected_waypoint[0])
        self.assertDictEqual(wpt_object['waypoint'][1], expected_waypoint[1])

    def test_parse_wpt_should_parse_two_lines_file_version_1_0(self):
        file_data = "OziExplorer Waypoint File Version 1.0\n" \
                    " WGS 84\n" \
                    " Reserved 2\n" \
                    " Reserved 3\n" \
                    "   4,B01054        ,  46.133389,   1.833500,36674.82502, 0, 1, 3, 0, 65535,CHIROUX                                 , 0, 0, 0 , 1778\n" \
                    "   19,S11040        ,  46.182999,   1.847000,36674.82502, 0, 1, 3, 0, 65535,AIRE MONT GUERET                        , 0, 0, 0 , 1312"

        wpt_object = wpt.loads(file_data)

        expected_waypoint = [
            {"index": 4, "name": "B01054", "latitude": 46.133389, "longitude": 1.8335, "date": "36674.82502",
             "symbol": 0, "status": 1, "mapDisplayFormat": 3, "foregroundColor": 0,
             "backgroundColor": 65535, "description": "CHIROUX", "pointerDirection": 0,
             "garminDisplayFormat": 0, "proximity": 0, "altitude": 1778},
            {"index": 19, "name": "S11040", "latitude": 46.182999, "longitude": 1.847000, "date": "36674.82502",
             "symbol": 0, "status": 1, "mapDisplayFormat": 3, "foregroundColor": 0,
             "backgroundColor": 65535, "description": "AIRE MONT GUERET", "pointerDirection": 0,
             "garminDisplayFormat": 0, "proximity": 0, "altitude": 1312}
            ]

        self.assertEqual(wpt_object['ozi_version'], "1.0", "Ozi version is wrong")
        self.assertEqual(wpt_object['geodesic_system'], "WGS 84", "Geodesic system is wrong")
        self.assertDictEqual(wpt_object['waypoint'][0], expected_waypoint[0])
        self.assertDictEqual(wpt_object['waypoint'][1], expected_waypoint[1])

    def test_load_wpt_should_load_two_lines_file_version_1_1_and_ignore_empty_line(self):
        file_data = "OziExplorer Waypoint File Version 1.1\n" \
                    " WGS 84\n" \
                    " Reserved 2\n" \
                    " Reserved 3\n" \
                    " 1,D01089,45.471114,1.842460,,0,1,3,0,65535,Deco SE Suc au may,0,0,0,2923,6,0,17 \n" \
                    "\n" \
                    " 16,A01062,45.459446,1.848441,,0,1,3,0,65535,Attero Freysselines,0,0,1000,2034,6,0,17"

        wpt_object = wpt.loads(file_data)

        expected_waypoint = [{"index": 1, "name": "D01089", "latitude": 45.471114, "longitude": 1.84246,
                              "symbol": 0, "status": 1, "mapDisplayFormat": 3, "foregroundColor": 0,
                              "backgroundColor": 65535, "description": "Deco SE Suc au may", "pointerDirection": 0,
                              "garminDisplayFormat": 0, "proximity": 0, "altitude": 2923, "fontSize": 6, "fontStyle": 0,
                              "symbolSize": 17},
                             {"index": 16, "name": "A01062", "latitude": 45.459446, "longitude": 1.848441,
                              "symbol": 0, "status": 1, "mapDisplayFormat": 3, "foregroundColor": 0,
                              "backgroundColor": 65535, "description": "Attero Freysselines", "pointerDirection": 0,
                              "garminDisplayFormat": 0, "proximity": 1000, "altitude": 2034, "fontSize": 6,
                              "fontStyle": 0,
                              "symbolSize": 17}
                             ]

        self.assertEqual(wpt_object['ozi_version'], "1.1", "Ozi version is wrong")
        self.assertEqual(wpt_object['geodesic_system'], "WGS 84", "Geodesic system is wrong")
        self.assertDictEqual(wpt_object['waypoint'][0], expected_waypoint[0])
        self.assertDictEqual(wpt_object['waypoint'][1], expected_waypoint[1])

