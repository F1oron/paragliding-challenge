from unittest import TestCase

from csvutils import build_wpt_content_from_csv, find_ethercalc_url_api_from_page_url


class Test(TestCase):
    def test_build_wpt_content_from_csv(self):
        csv_content =   "Index,Label,Latitude,Longitude,,,,,,,Description,,,Rayon,Altitude,,,\n"\
                        "1,D01089,45.471114,1.84246,,0,1,3,0,65535,Deco SE Suc au may,0,0,300,2923,6,0,17\n"\
                        "2,D02087,45.468135,1.850644,,0,1,3,0,65535,Deco SO Fournaise,0,0,300,2822,6,0,17\n" \
                        "14,A01062,45.459446,1.848441,,0,1,3,0,65535,Attero Freysselines,0,0,300,2034,6,0,17\n"\
                        "42,B15058,45.477019,1.792404,,0,1,3,0,65535,Cimetierre de Madranges,0,0,350,1932,6,0,17\n"\
                        "43,B16073,45.555083,1.890479,,0,1,3,0,65535,Gourdon murat centre,0,0,500,2388,6,0,17"

        expected_wpt_content = "OziExplorer Waypoint File Version 1.1\n"\
                            "WGS 84\n"\
                            "Reserved 2\n"\
                            "Reserved 3\n" \
                            "1,D01089,45.471114,1.84246,,0,1,3,0,65535,Deco SE Suc au may,0,0,300,2923,6,0,17\n" \
                            "2,D02087,45.468135,1.850644,,0,1,3,0,65535,Deco SO Fournaise,0,0,300,2822,6,0,17\n" \
                            "14,A01062,45.459446,1.848441,,0,1,3,0,65535,Attero Freysselines,0,0,300,2034,6,0,17\n" \
                            "42,B15058,45.477019,1.792404,,0,1,3,0,65535,Cimetierre de Madranges,0,0,350,1932,6,0,17\n" \
                            "43,B16073,45.555083,1.890479,,0,1,3,0,65535,Gourdon murat centre,0,0,500,2388,6,0,17"
        self.assertEqual(expected_wpt_content, build_wpt_content_from_csv(csv_content))

    def test_build_content_from_csv_when_some_parameters_have_quotes(self):
        csv_content = 'Index,Label,Latitude,Longitude,,,,,,,Description,,,Rayon,Altitude,,,\n' \
                   '1,D01089,45.471114,1.84246,,0,1,3,0,65535,Deco SE Suc au may,0,0,300,2923,6,0,17\n' \
                   '2,D02087,45.468135,1.850644,,0,1,3,0,65535,"Deco SO Fournaise",0,0,300,2822,6,0,17\n' \
                   '14,A01062,45.459446,1.848441,,0,1,3,0,65535,Attero Freysselines,0,0,300,2034,6,0,17\n' \
                   '42,B15058,45.477019,1.792404,,0,1,3,0,65535,Cimetierre de Madranges,0,0,350,1932,6,0,17\n' \
                   '43,B16073,45.555083,1.890479,,0,1,3,0,65535,Gourdon "murat" centre,0,0,500,2388,6,0,17'
   
        expected_wpt_content = 'OziExplorer Waypoint File Version 1.1\n' \
                            'WGS 84\n' \
                            'Reserved 2\n' \
                            'Reserved 3\n' \
                            '1,D01089,45.471114,1.84246,,0,1,3,0,65535,Deco SE Suc au may,0,0,300,2923,6,0,17\n' \
                            '2,D02087,45.468135,1.850644,,0,1,3,0,65535,Deco SO Fournaise,0,0,300,2822,6,0,17\n' \
                            '14,A01062,45.459446,1.848441,,0,1,3,0,65535,Attero Freysselines,0,0,300,2034,6,0,17\n' \
                            '42,B15058,45.477019,1.792404,,0,1,3,0,65535,Cimetierre de Madranges,0,0,350,1932,6,0,17\n' \
                            '43,B16073,45.555083,1.890479,,0,1,3,0,65535,Gourdon "murat" centre,0,0,500,2388,6,0,17'
        self.assertEqual(expected_wpt_content, build_wpt_content_from_csv(csv_content))

    def test_build_ethercalc_url(self):
        self.assertEqual("https://lite.framacalc.org/_/balises-challenge-5cr8/csv",
                         find_ethercalc_url_api_from_page_url("https://lite.framacalc.org/balises-challenge-5cr8"))
