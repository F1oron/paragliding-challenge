from unittest import TestCase

from Task import Task
from WptParser import WptParser
import wpt
from GpsPoint import GpsPoint
from Turnpoint import TaskPoint


class TestWptParser(TestCase):
    def test_parse_wpt_file(self):
        file_data = "OziExplorer Waypoint File Version 1.1\n" \
                    " WGS 84\n" \
                    " Reserved 2\n" \
                    " Reserved 3\n" \
                    " 1,D01089,45.471114,1.842460,,0,1,3,0,65535,Deco SE Suc au may,0,0,1000,2923,6,0,17 \n" \
                    " 2,D02087,45.468135,1.850644,,0,1,3,0,65535,Deco SO Fournaise,0,0,2000,2822,6,0,17 \n" \
                    " 3,D03090,45.486021,1.839391,,0,1,3,0,65535,Deco NO puy de la Monediere,0,0,1500,2953,6,0,17\n " \
                    "22,A08044,45.997783,1.451967,,0,1,3,0,65535,Attero la Jonchere,0,0,800,1444,6,0,17\n" \
                    "23,A09036,46.067750,1.451650,,0,1,3,0,65535,Attero St sulpice ter,0,0,1200,1145,6,0,17\n" \
                    "43,B05070,45.499194,1.827970,,0,1,3,0,65535,Puy D'orliac,0,0,1000,2297,6,0,17 \n" \
                    "44,B06073,45.506090,1.808183,,0,1,3,0,65535,Pierre des druides,0,0,3000,2402,6,0,17\n " \
                    "45,B07077,45.510733,1.802876,,0,1,3,0,65535,Puy pantout,0,0,1500,2526,6,0,17\n" \
                    "46,B08076,45.512883,1.810936,,0,1,3,0,65535,Puy de la roche,0,0,300,2493,6,0,17"
        parser = WptParser(None)
        wpt_object = wpt.loads(file_data)
        task = parser.extract_wpt_informations(wpt_object)

        expected_take_off = [TaskPoint(label="D01089", coordinates=GpsPoint(45.471114, 1.842460), radius=1000, description="Deco SE Suc au may", altitude=2923),
                             TaskPoint(label="D02087", coordinates=GpsPoint(45.468135, 1.850644), radius=2000, description="Deco SO Fournaise", altitude=2822),
                             TaskPoint(label="D03090", coordinates=GpsPoint(45.486021, 1.839391), radius=1500, description="Deco NO puy de la Monediere", altitude=2953)]
        expected_landings = [TaskPoint(label="A08044", coordinates=GpsPoint(45.997783, 1.451967), radius=800, description="Attero la Jonchere", altitude=1444),
                             TaskPoint(label="A09036", coordinates=GpsPoint(46.067750, 1.451650), radius=1200, description="Attero St sulpice ter", altitude=1145)]
        expected_turnpoints = [TaskPoint(label="B05070", coordinates=GpsPoint(45.499194, 1.827970), radius=1000, description="Puy D'orliac", altitude=2297),
                               TaskPoint(label="B06073", coordinates=GpsPoint(45.506090, 1.808183), radius=3000, description="Pierre des druides", altitude=2402),
                               TaskPoint(label="B07077", coordinates=GpsPoint(45.510733, 1.802876), radius=1500, description="Puy pantout", altitude=2526),
                               TaskPoint(label="B08076", coordinates=GpsPoint(45.512883, 1.810936), radius=300, description="Puy de la roche", altitude=2493)]

        expected_task = Task(expected_take_off)
        self.assertEqual(expected_task, task)
