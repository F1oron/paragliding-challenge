from unittest import TestCase

from bonus import Bonus


class TestBonus(TestCase):
    def test_bonus_init_with_value(self):
        bonus = Bonus({"A": 1.8, "B": 1.6, "C": 1.4, "D": 1.2, "CCC": 1}, 1.5)
        self.assertEqual(1.5, bonus.fly_loop)
        self.assertDictEqual({"A": 1.8, "B": 1.6, "C": 1.4, "D": 1.2, "CCC": 1}, bonus.wing_categories)

    def test_bonus_init_with_empty_arguments(self):
        bonus = Bonus()
        self.assertEqual(1, bonus.fly_loop)
        self.assertDictEqual({"A": 1, "B": 1, "C": 1, "D": 1, "CCC": 1}, bonus.wing_categories)

    def test_bonus_should_return__when_wing_is_A_fly_make_loop_and_pilot_is_man(self):
        bonus = Bonus({"A": 1.8, "B": 1.6, "C": 1.4, "D": 1.2, "CCC": 1}, 1.5)
        self.assertEqual(2.7, bonus.compute_bonus('A', True))
