# Paragliding challenge
## Introduction
Ce dépôt contient un script python permettant le calcul de points lors d'un challenge cross de parapente.

Le principe de ce type de challenge est de proposer plusieurs balises à atteindre sur une période donnée, chaque balise vaut un certain nombre de point.

Le script propose d'ajouter des bonus en fonction de la catégorie de la voile ou pour les vols bouclés.

Le format de sortie est au choix, un fichier texte de la forme suivante:

```
Résultats du challenge cross organisé du 01/09/2019 au 31/08/2021

Le challenge est constitué de 54 balises à franchir, les coefficients multiplicateurs suivants sont appliqués :
Voile A : 1.7
Voile B : 1.5
Voile C : 1.3
Voile D : 1.1
Voile CCC : 1.0
Parcours bouclé : 1.5

Le classement est le suivant :
Place | Pilote                     | Nombre de points | Nombre de balises validées
------|----------------------------|------------------|---------------------------
1     | Arthur Pendragon           | 170.27           | 23                        
2     | Provencal le Gaullois      | 97.25            | 13                        
3     | Karadoc de Vannes          | 84.88            | 10                        
4     | Lancelot du Lac            | 78.38            | 8                         
5     | Bohort de Gaunes           | 64.86            | 7                         
6     | Yvain le chevalier au Lion | 34.57            | 6                         
```

ou un fichier CSV :

```csv
Classement,Pilote,Points,NbBalises,B01061,B02058,B03053,B04054,B05070,B06073,B07077,B08076,B09059,B10059,B11060,B12055,B13034,B14055,B15058,B16073,B17083,B18044,B19041,B20051,B21074,B22073,B23057,B24059,B25065,B26089,B27081,B28058,B29059,B30054,B31057,B32056,B33052,B34046,B35041,B36043,B37053,B38056,B39057,B40097,B41075,B42080,B43050
1,Arthur Pendragon,107.05,20,1.75,2.15,4.04,3.73,2.8,5.44,6.53,6.15,4.26,6.25,10.94,11.4,,9.82,4.17,,,9.01,,,,4.42,4.52,,2.23,0.59,,6.83,,,,,,,,,,,,,,,
2,Provencal le Gaullois,61.2,12,1.75,2.15,4.04,3.73,2.8,,4.36,,4.26,,,,,,6.25,,,13.51,,,,,,5.73,3.35,,,,,,,,9.27,,,,,,,,,,
3,Karadoc de Vannes,52.57,10,2.39,2.93,,3.39,3.82,7.42,8.91,8.39,,,,,,,5.69,,,,,,,,,,,1.22,,,,,8.42,,,,,,,,,,,,
4,Lancelot du Lac,36.37,8,2.07,2.54,4.78,4.41,2.21,,7.72,,,,,,,,7.39,,,,,,,,,,,,,,5.27,,,,,,,,,,,,,,
5,Bohort de Gaunes,6.79,2,,,,,,,,,,,,,,,,,,,,,,,,,2.23,,,4.55,,,,,,,,,,,,,,,
6,Yvain le chevalier au Lion,0.81,1,,,,,,,,,,,,,,,,,,,,,,,,,,0.81,,,,,,,,,,,,,,,,,
```

## Pré-requis
Python 3.x

Packages python : geopy, requests, coverage

### Windows
Télécharger et installer [Python](https://www.python.org/downloads/windows/) en version supérieure à 3

Dans une invite de commande : `python pip install geopy requests`

### Ubuntu
`sudo apt install python3 python3-geopy python3-requests`

### Manjaro / Arch Linux
`sudo pacman -S python python-geopy python-requests`

## Configuration
Plusieurs fichiers de configuration sont nécessaires :
- Un fichier de définition des balises au format CSV, exemple :
```
lat,lon,Type,Description,Altitude_m,Rayon,Score
45.471114,1.84246,takeoff,Exemple de decollage,890,300,
45.468135,1.850644,landing,Exemple d atterrissage,860,300,
45.486021,1.839391,turnpoint,Exemple de balise,900,400,5.6
```

La colonne `Type` accèpte les valeurs suivantes : `takeoff` (décollage), `landing` (atterrissage) et `turnpoint` (balise).
Le rayon désigne le rayon en mètres du cercle autours d'un point.
Si le score est vide, un score va être calculé, égal à la distance en kilomètres entre la balise et le décollage le plus proche.

- Un fichier contenant les règles du challenge (date de début / fin, bonus) au format CSV
```csv
Category;Attribut;Valeur
Date;Start;01/09/2020
Date;End;31/08/2021
WingBonus;A;1.7
WingBonus;B;1.5
WingBonus;C;1.3
WingBonus;D;1.1
WingBonus;CCC;1
LoopBonus;Yes;1.5
LoopBonus;No;1
```

## Ajout d'un vol
Le format de fichier des traces pris en charge est le format IGC.

Le format du nom de fichier doit répondre à la spécification suivante :
`{DATE}_{PRENOM}_{NOM}_{CATEGORIE VOILE}_{CLUB}-{DIVERS}.igc`

* {DATE} : Date du vol au format AAAA-MM-JJ
* {PRENOM} et {NOM} : Prénom et nom du pilote
* {CATEGORIE VOILE} : Choix disponible : A, B, C, D, CCC
* {CLUB} : Initiales du club, ex : TVL pour Thermiques Verts du Limousin
* {DIVERS} : tout autres informations qui ne seront pas prises en compte

Les fichiers sont à regrouper dans un seul répertoire.

## Utilisation du script
Il existe deux manières de fournir les données d'entrée au script.
### Définition des chemins de tous les fichiers de configuration
```bash
python paragliding-challenge.py compute_score --tracks {chemin_répertoire_traces} --task {chemin_vers_fichier_definition_des_balises} --rules {chemin_vers_fichier_de_règles} --out {nom_du_fichier_de_sortie}
```

### Utilisation d'une archive
Si l'ensemble des fichiers du challenge sont dans une archive (format supporté : ZIP), il est possible d'utiliser la commande suivante :
```bash
python src/paragliding-challenge.py compute_score --archive {chemin_vers_archive} --out {nom_du_fichier_de_sortie}
```

L'architecture des fichiers dans l'archive doit être la suivante :
archive.zip
```
Challenge/
- rules.csv
- balises.csv
Flights/
- vol1.igc
- vol2.igc
- ...
```

Un dossier de template est disponible sur ce dépôt.

## Conversion de fichier balises
Une méthode permet de convertir les fichiers de balises au format geojson, permettant l'affichage sur une carte de type [umap](https://umap.openstreetmap.fr).

### Fichier WPT vers GEOJSON

```
python src/paragliding-challenge.py convert --project_file {chemin_vers_fichier_definition_balises}.csv
```

Le fichier de sortie sera de même nom que celui d'entré, placé dans le même répertoire mais avec une extension en .geojson.
- `--circle` : Les balises ponctuelles sont remplacées par des cercles.
- `--seggregate` : Au lieu de générer un fichier geojson, trois fichiers contenant les décollages, les atterrissages et les balises sont écrit avec comme noms : `{chemin_vers_fichier_balise}_takeoff.geojson`, `{chemin_vers_fichier_balise}_landing.geojson` et `{chemin_vers_fichier_balise}_turnpoint.geojson`.

### Fichier CVS vers GEOJSON
```
python src/paragliding-challenge.py convert --waypoint {chemin_vers_fichier_definition_balises}.csv
```

Les arguments `--score_file` `--circle` et `--segregate` sont aussi utilisables.

Deux fichiers sont générés, un fichier .wpt et un .geojson dans le même répertoire que le fichier balise.

### Import et conversion d'un classeur ethercalc en geojson
Ethercalc est un logiciel permettant de contribuer à plusieurs en ligne à une feuille de calcul et permettant l'export du fichier au format csv.
Différentes instances existent tel que : [ethercalc](https://ethercalc.net/) ou [framacalc](https://framacalc.org/abc/fr/)

```
python src/paragliding-challenge.py convert --waypoint {chemin_vers_fichier_definition_balises}.csv --ethercalc_url {url}
```

Cette commande télécharge les données du tableau au format csv (voir exemple ci-dessus), les convertit au format wpt et l'enregistre à l'emplacement indiqué par l'argument `--waypoint`, puis convertit ce fichier au format geojson.

Les arguments `--score_file`, `--circle` et `--segregate` sont aussi utilisables.

## Tests unitaires
Ajouter les dossiers `src` et `test` au PYTHONPATH.

### Lancement des tests
```
cd test
python -m unittest
```

### Couverture de tests
Installer le module python `coverage`

#### Windows
```
python pip install coverage
```

#### Manjaro
```
pacman -S python-coverage
```

#### Tests et affichage
```
cd test
coverage run -m unittest
coverage report -m
```

## Documentation ligne de commande
```
usage: paragliding-challenge.py [-h] {compute_score,convert} ...

positional arguments:
  {compute_score,convert}
                        sub-command help
    compute_score       Compute challenge score
    convert             Convert waypoint files

options:
  -h, --help            show this help message and exit
```

```
usage: paragliding-challenge.py compute_score [-h] [-a ARCHIVE]
                                              [--tracks TRACKS] [--task TASK]
                                              [--rules RULES] [-o OUT] [-v]

options:
  -h, --help            show this help message and exit
  -a ARCHIVE, --archive ARCHIVE
                        Path to archive which contains all challenge files
  --tracks TRACKS       Path to tracks directory (if archive isn't used)
  --task TASK           Path to task file. CSV file containing waipoints and
                        scores (if archive isn't used)
  --rules RULES         Path to the file containing rules of the challenge (if
                        archive isn't used)
  -o OUT, --out OUT     Path to output CSV file
  -v, --verbose         Print script working informations
```

```
usage: paragliding-challenge.py convert [-h] -t TASK [-c] [--segregate]
                                        [-e ETHERCALC] [-v]

options:
  -h, --help            show this help message and exit
  -t TASK, --task TASK  CSV file containing waipoints and scores to convert
  -c, --circle          Display waipoints as circle instead of points
  --segregate           Output will be divide in three files, one per waypoint
                        type (takeoff, landing, turnpoint)
  -e ETHERCALC, --ethercalc ETHERCALC
                        URL of a Ethercalc page to export CSV file
  -v, --verbose         Print script working informations
```